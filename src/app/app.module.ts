import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { Tab2Page} from  '../app/tab2/tab2.page';
import { Tab1Page } from '../app/tab1/tab1.page';
import { Tab3Page } from '../app/tab3/tab3.page';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { WeatherService } from '../app/weather.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { IonicStorageModule} from '@ionic/Storage';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, HttpClientModule, IonicModule.forRoot(),IonicStorageModule.forRoot() , AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }, WeatherService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
