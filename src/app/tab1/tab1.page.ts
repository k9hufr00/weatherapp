import {Component, OnInit, ElementRef, NgZone, ViewChild} from "@angular/core";
import {Router} from "@angular/router";
import {WeatherService} from "../../app/weather.service";
import {Geolocation} from '@ionic-native/geolocation/ngx';
import {HttpClient} from "@angular/common/http";
import { logging } from 'protractor';
import { Storage } from '@ionic/Storage';



declare var google: any; 

@Component( {
  selector: "app-tab1",
  templateUrl: "tab1.page.html",
  styleUrls: [  "tab1.page.scss" ]
} )
export class Tab1Page {
  @ViewChild('Map', {static: false}) mapElement: ElementRef;
  map: any; 
  mapOptions: any; 
 /* locationMunich= {lat: 48.137154, lng: 11.576124};*/
  markerOptions: any = {position: null, map: null, tilte: null};
  marker: any;
  apiKey: any = 'AIzaSyCSF_MtiPRZaG6lkHwhzyKVlNVNuTi5ilc';


  currentWeather: any[] = [];
  temperature: number;
  humidity: number;
  pressure: number;
  clouds: number;
  lng: number;
  lat: number;
  description: string;

  /*location: {
    city: string;
    state: string;
  };*/
  location: any; 
 

  constructor( private weatherService: WeatherService, private router: Router, public zone: NgZone, public geolocation: Geolocation, public storage: Storage) { }
    

  ionViewWillEnter() {
    this.storage.get('location').then(data => {
      console.log(data);
      
      this.location = JSON.parse(data);
      this.weather();
      this.maps();
    })
  }

  weather() {
    console.log('weather');
    
    this.weatherService
      .getWeather(this.location.lat, this.location.lng)
      .subscribe( resData => {
        this.temperature = parseInt( ( resData.main.temp - 273.15 ).toFixed( 2 ) ),
          this.clouds = resData.clouds.all,
          this.pressure = resData.main.pressure,
          this.humidity = resData.main.humidity,
          this.lat = resData.coord.lat,
          this.lng = resData.coord.lng,
          this.description = resData.weather[ 0 ].description
      } );
    // REFACTORING: adjusted usage of weather service and stored data of GET request to responsible fields
  }

  maps() {
    console.log('maps');
    
    const script = document.createElement('script');
    script.id = 'googleMap';
    if(this.apiKey){
      script.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey;
    }
    else{
      script.src = 'https://maps.googleapis.com/maps/api/js?key=';
    }
    document.head.appendChild(script);
    /* this.geolocation.getCurrentPosition().then((position) =>  {
      this.location.lat = position.coords.latitude;
      this.location.lng = position.coords.longitude;
    }); */

    this.mapOptions = {
      center: this.location,
      zoom: 8,
      mapTypeControl: false
    }; 

    setTimeout(() =>{
      this.map = new google.maps.Map
      (this.mapElement.nativeElement, this.mapOptions);
      this.markerOptions.position = this.location;
      this.markerOptions.map = this.map;
      this.markerOptions.tilte = 'My Location';
      this.marker = new google.maps.Marker 
      (this.markerOptions);
    }, 3000);
  }
}


