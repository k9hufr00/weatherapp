import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from "rxjs/operators";
import { Storage } from "@ionic/Storage";


@Injectable( {
  providedIn: 'root'
} )
export class WeatherService {
  apiKey = 'a6ef0217de5ffac9ad224e040ce23f91';
  
  urlPrevious = 'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid=a6ef0217de5ffac9ad224e040ce23f91';
  url = 'https://api.openweathermap.org/data/2.5/weather?';

  constructor( public http: HttpClient, private storage: Storage ) {
  }

  /*getData(url): Observable<any> {
    const address = `${API_URL}/${url}&apikey=${API_KEY}`;
    return this.http.get(address);
    .map(res => res.json());
  }*/

  getWeather ( lat: number, lng: number ) {
    var getFromUrl = this.urlBuilder( lat, lng, this.apiKey );
    console.log( 'buil API call: ', getFromUrl );
    return this.http.get<any>( getFromUrl );
  }

  private urlBuilder ( lat: number, lng: number, apiKey: string ) {
    var locationString = 'lat=' + lat + '&lon=' + lng;
    var apiKeyString = 'appid=' + apiKey;
    return this.url + locationString + '&' + apiKeyString;
  }
  // REFACTORING 1: city and state are strings. Now, the method getWeather() also expect these values 
  // to be strings
  // REFACTORING 2: adjusted API call
}