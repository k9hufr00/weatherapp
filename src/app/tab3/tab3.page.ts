import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { Storage } from '@ionic/Storage';
import { NavController } from '@ionic/angular';
import { Tab1Page } from '../tab1/tab1.page';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  celsius: number; 
  fahrenheit: number;
  lat: number;
  lng: number; 

  constructor(private storage: Storage, private navCtrl: NavController) {
    this.storage.get('location').then((val) => {
      if(val != null){
        let location = JSON.parse(val);
        this.lat = location.lat;
        this.lng = location.lng;
      }else {
        this.lat = 48.13641;
        this.lng = 11.57754;
      }
    });
  }

  saveForm(){
    console.log('hallo');
    
    let location = {
     lat: this.lat,
     lng: this.lng,
    }
    this.storage.set('location', JSON.stringify(location));
  }


  ngOnInit(){
    this.celsius = 0;
    this.fahrenheit = this.celsius * 9 / 5 + 32;
  }

  celsius_changed(){
    this.fahrenheit = this.celsius * 9 / 5 + 32;
  }

  fahrenheit_changed(){
    this.celsius = (this.fahrenheit - 32) * 5 / 9;
  }
}
